## DevuX
## Distribution Linux pour développeurs  
  

![Logo](logo.png)

Créée à partir de l'outil **live-build**, DevuX est une distribution Linux basée sur **Debian** Bullseye utilisant l'environnement de bureau **MATE**.  

Le but est d'avoir un système Linux avec les outils pour faire du développement, IDE, outils et serveur web.  

Développé par [Patrice ANDREANI](https://andre-ani.fr/devux-linux/).

Sources disponibles sur [Gitlab](https://gitlab.com/andre0ani1/devux).

Version actuelle :

**1.0** 


Version **beta**, ne pas utiliser en production.

